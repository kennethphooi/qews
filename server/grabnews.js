var FeedParser = require('feedparser');
var request = require('request'); // for fetching the feed


// sequelize to news_site then for loop 
// grab news feed
// sequelize insert to the news_article by date
//for  from news site using sequelize
function zaobao(){
  var req = request('http://www.zaobao.com/ssi/rss/sp.xml')
  var feedparser = new FeedParser();

  req.on('error', function (error) {
    // handle any request errors
  });

  req.on('response', function (res) {
    var stream = this; // `this` is `req`, which is a stream

    if (res.statusCode !== 200) {
      this.emit('error', new Error('Bad status code'));
    }
    else {
      stream.pipe(feedparser);
    }
  });

  feedparser.on('error', function (error) {
    // always handle errors
  });

  feedparser.on('readable', function () {
    // This is where the action is!
    var stream = this; // `this` is `feedparser`, which is a stream
    var meta = this.meta; // **NOTE** the "meta" is always available in the context of the feedparser instance
    var item;

    var obj = {
      table: []
    };


    while (item = stream.read()) {
      // rss items
      /*
      console.log("***** " + item.title + "***** ");
      console.log(item.link);
      console.log(item.author);
      console.log("***** ******** ***** ");
      */
      console.log(item.title);
      console.log(item.description);
    }
  });
}

function cna(){
  return new Promise(function (resolve, reject) {
    var req2 = request('http://www.channelnewsasia.com/rssfeeds/8396082')
    var feedparser2 = new FeedParser();

    req2.on('error', function (error) {
      // handle any request errors
      reject('error')
    });

    req2.on('response', function (res) {
      var stream = this; // `this` is `req`, which is a stream

      if (res.statusCode !== 200) {
        this.emit('error', new Error('Bad status code'));
      }
      else {
        stream.pipe(feedparser2);
      }
    });

    feedparser2.on('error', function (error) {
      // always handle errors
    });

    feedparser2.on('readable', function () {
      // This is where the action is!
      var stream = this; // `this` is `feedparser`, which is a stream
      var meta = this.meta; // **NOTE** the "meta" is always available in the context of the feedparser instance


      resolve(stream);
    });
  })
}

module.exports = {
  zaobao: zaobao,
  cna: cna
}