var Sequelize = require("sequelize");

module.exports = function(database) {
  return database.define('news', {
    id: {
      type: Sequelize.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    // created_at: {
    //   type: Sequelize.DATE,
    //   allowNull: false,
    // },
    // updated_at: {
    //   type: Sequelize.DATE,
    //   allowNull: false,
    // },  
    news_site_id: {
      type: Sequelize.INTEGER(10).UNSIGNED,
      allowNull: false,
    },
    article: {
      type: Sequelize.STRING,
      allowNull: false
        },
    creditable: {
      type: Sequelize.INTEGER(10).UNSIGNED,
      allowNull: false,
    },
    article_published_at: {
      type: Sequelize.DATEONLY,
      allowNull: false,
    },
    news_title: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    article_url: {
      type: Sequelize.STRING(500),
      allowNull: false,
    },
     
  }, {
 
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
    freezeTableName: true
  });
};
