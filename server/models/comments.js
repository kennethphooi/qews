var Sequelize = require("sequelize");

module.exports = function(database) {
  return database.define('comments', {
    id: {
      type: Sequelize.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
    },
    // created_at: {
    //   type: Sequelize.DATE,
    //   allowNull: false,
    // },
    // updated_at: {
    //   type: Sequelize.DATE,
    //   allowNull: false,
    // },  
    news_id: {
      type: Sequelize.INTEGER(10),
      allowNull: false,
      references: {
        model: 'news',
        key: 'id'
      },
    },
    user_id: {
      type: Sequelize.INTEGER(10),
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      },
    },
  },
  {
    timestamps: true,
    createdAt: "created_at",
    updatedAt: "updated_at",
    freezeTableName: true,
  });
};
