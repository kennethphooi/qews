(function() {
    angular
        .module('qews', ['irontec.simpleChat',"ui.router", "ngFlash", "ngSanitize", "ngProgress"]);
})();