(function () {
    angular
        .module("qews")
        .controller("MainCtrl", MainCtrl);

    MainCtrl.$inject =['$state', 'Flash', 'ngProgressFactory', 'GrabNewsSvc']

    function MainCtrl($state, Flash, ngProgressFactory, GrabNewsSvc){
        var vm = this;

        vm.progressbar = ngProgressFactory.createInstance();

        vm.messages = [];

       init();
        function init() {
            GrabNewsSvc.grabNews()
            .then(function (articles){
                vm.messages = articles.data
            })
        }
           
        

        
        vm.visible = true;
        vm.expandOnNew = true;
        
    }
})();