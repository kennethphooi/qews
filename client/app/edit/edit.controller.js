(function () {
    'use strict';
    angular
        .module("qews")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$filter", "PassportSvc", "$stateParams"];

    function EditCtrl($filter, PassportSvc, $stateParams) {

        // Declares the var vm (for ViewModel) and assigns it the object this. Any function or variable that you attach
        // to vm will be exposed to callers of EditCtrl, e.g., edit.html
        var vm = this;

        // Exposed data models -----------------------------------------------------------------------------------------
        // Creates a department object. We expose the department object by attaching it to the vm. This allows us to
        // apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        vm.username = "";
        vm.result = {};

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
        vm.deleteManager = deleteManager;
        vm.initDetails = initDetails;
        vm.search = search;
        vm.toggleEditor = toggleEditor;
        vm.updateDetails = updateDetails;


        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        initDetails();

        // Saves edited department name
        function updateDetails() {
            console.log("UpdateDetails1()");
            PassportSvc
                .updateDetails(vm.username, vm.result.name_first, vm.result.name_last)
                .then(function (result) {
                    console.log("UpdateDetails Success" + JSON.stringify(result.data));
                })
                .catch(function (err) {
                    console.log("UpdateDetails Failure" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }



        if ($stateParams.username) {
            vm.username = $stateParams.username;
            vm.search()
        };

        // Function declaration and definition -------------------------------------------------------------------------
        // Deletes displayed manager. Details of preceding manager is then displayed.
        function deleteManager() {
            DeptService
                .deleteDept(vm.dept_no, vm.result.manager_id)
                .then(function (response) {
                    // Calls search() in order to populate manager info with predecessor of deleted manager
                    search();
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error: \n" + JSON.stringify(err));
                });
        }

        // Initializes department details shown in view
        function initDetails() {
            console.log("initDetails()");
            vm.result.username= "";
            vm.result.name_first= "";
            vm.result.name_last= "";
            vm.showDetails = false;
            vm.isEditorOn = false;
        }



        // Given a department number, this function searches the Employees database for
        // the department name, and the latest department manager's id/name and tenure
        function search() {
            console.log("search()");
            initDetails();
            vm.showDetails = true;

            PassportSvc
                .retrieveUser(vm.username)
                .then(function (result) {
                    // Show table structure
                    vm.showDetails = true;

                    // This is a good way to understand the type of results you're getting
                    console.log("search() successful" + JSON.stringify(result.data));
                    console.log(result.data[0].username);
                    // Exit .then() if result data is empty
                    if (!result.data)
                        return;

                    // The result is an array of objects that contain only 1 object
                    // We are assigning value like so, so that we don't have to do access complex structures
                    // from the view. Also this would give you a good sense of the structure returned.
                    // You could, of course, massage data from the back end so that you get a simpler structure
                    vm.result.username = result.data[0].username;
                    if (result.data[0].username) {

                        vm.result.name_first= result.data[0].name_first;
                        vm.result.name_last= result.data[0].name_last
                    }
                })
                .catch(function (err) {
                    console.log("search() error" + JSON.stringify(err));
                });
        }

        // Switches editor state of the department name input/edit field
        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }
    }
})();