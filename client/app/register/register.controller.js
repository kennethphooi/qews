(function () {
    angular
        .module("qews")
        .controller("RegisterCtrl", RegisterCtrl);

        RegisterCtrl.$inject = ["$sanitize", "$state", "Flash", "AuthFactory"];

    function RegisterCtrl($sanitize, $state, Flash, AuthFactory){
        var vm = this;
        vm.emailAddress = "";
        vm.firstName = "";
        vm.lastName = "";
        vm.password = "";
        vm.confirmpassword = "";

        vm.register = function () {
            AuthFactory.register($sanitize(vm.emailAddress), $sanitize(vm.password), $sanitize(vm.firstName), $sanitize(vm.lastName), $sanitize(vm.confirmpassword))
                .then(function () {
                    vm.disabled = false;

                    vm.emailAddress = "";
                    vm.password = "";
                    vm.confirmpassword = "";
                    Flash.clear();
                    Flash.create('success', "Successfully sign up with us, Please proceed to login", 0, {class: 'custom-class', id: 'custom-id'}, true);
                    $state.go("signIn");
                }).catch(function () {
                console.error("registration having issues");
            });
        };

    }
})();