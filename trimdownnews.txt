***** Thorny affair: How I survived the Durian Run***** 
http://www.channelnewsasia.com/news/singapore/thorny-affair-how-i-survived-the-durian-run-9055808
Diane Leow
***** ******** ***** 
***** Swimming: Schooling sets new Asian, national record for 50m fly at world championships***** 
http://www.channelnewsasia.com/news/singapore/swimming-schooling-sets-new-asian-national-record-for-50m-fly-at-9056120
null
***** ******** ***** 
***** 7 in 10 Singaporean seniors visited by Pioneer Generation Ambassadors***** 
http://www.channelnewsasia.com/news/singapore/7-in-10-singaporean-seniors-visited-by-pioneer-generation-9055980
Loh Chuan Junn
***** ******** ***** 
***** 3-day raid in Geylang leads to 60 arrests***** 
http://www.channelnewsasia.com/news/singapore/3-day-raid-in-geylang-leads-to-60-arrests-9055960
null
***** ******** ***** 
***** ComfortDelGro to allow cashless digital payment for street-hail taxi rides***** 
http://www.channelnewsasia.com/news/singapore/comfortdelgro-to-allow-cashless-digital-payment-for-street-hail-9055916
null
***** ******** ***** 
***** ‘I’m not bad, I’m not ill, I’m autistic’: Woman's relief on being finally diagnosed at 42***** 
http://www.channelnewsasia.com/news/cnainsider/i-m-not-bad-i-m-not-ill-i-m-autistic-woman-s-relief-on-being-9055726
Desmond Ng
***** ******** ***** 
***** Buying your way to a degree: Singapore-based ghostwriting services target students online***** 
http://www.channelnewsasia.com/news/singapore/buying-your-way-to-a-degree-singapore-based-ghostwriting-9029638
Lianne Chia
***** ******** ***** 
***** Shanmugam warns of 'trust deficit' caused by past terrorist incidents***** 
http://www.channelnewsasia.com/news/singapore/shanmugam-warns-of-trust-deficit-caused-by-past-terrorist-9054782
Olivia Siong
***** ******** ***** 
***** Changi, a short film about immigration, wins top prize at 2017 National Youth Film Awards***** 
http://www.channelnewsasia.com/news/lifestyle/changi-a-short-film-about-immigration-wins-top-prize-at-2017-9054774
Genevieve Sarah Loh
***** ******** ***** 
***** Eisner Awards 2017: Graphic novelist Sonny Liew becomes first Singaporean to win at 'Oscars of comics'***** 
http://www.channelnewsasia.com/news/lifestyle/eisner-awards-2017-graphic-novelist-sonny-liew-becomes-first-9051868
Mayo Martin
***** ******** ***** 
***** Damaged pedestrian bridge beam at Balestier Road removed after accident: BCA***** 
http://www.channelnewsasia.com/news/singapore/damaged-pedestrian-bridge-beam-at-balestier-road-removed-after-9054502
null
***** ******** ***** 
***** The more successful Singapore is, the harder it is to attract political talent: Chan Chun Sing***** 
http://www.channelnewsasia.com/news/singapore/the-more-successful-singapore-is-the-harder-it-is-to-attract-9053284
null
***** ******** ***** 
***** Suspected operators of vice website Laksaboy arrested; 2 syndicates crippled***** 
http://www.channelnewsasia.com/news/singapore/suspected-operators-of-vice-website-laksaboy-arrested-2-9053486
null
***** ******** ***** 
***** Trailer rams Balestier Road pedestrian bridge***** 
http://www.channelnewsasia.com/news/singapore/trailer-rams-balestier-road-pedestrian-bridge-9053514
null
***** ******** ***** 
***** Suspected serial molester arrested for at least 10 cases in Hougang, Sengkang***** 
http://www.channelnewsasia.com/news/singapore/suspected-serial-molester-arrested-for-at-least-10-cases-in-9053226
null
***** ******** ***** 
***** 87 suspected drug offenders arrested in 4-day islandwide operation***** 
http://www.channelnewsasia.com/news/singapore/87-suspected-drug-offenders-arrested-in-4-day-islandwide-9053008
null
***** ******** ***** 
***** Retiree jailed 8 years for sexually assaulting 12-year-old boy***** 
http://www.channelnewsasia.com/news/singapore/retiree-jailed-8-years-for-sexually-assaulting-12-year-old-boy-9052768
Vanessa Paige Chelvan
***** ******** ***** 
***** Football: Singapore thrashed 7-0 by Australia in AFC U-23 qualifiers***** 
http://www.channelnewsasia.com/news/sport/football-singapore-thrashed-7-0-by-australia-in-afc-u-23-9052608
Amir Yusof
***** ******** ***** 
***** Commentary: The SAF’s offer to fight terror in Marawi and the hard-nosed calculations of a small state***** 
http://www.channelnewsasia.com/news/singapore/commentary-the-saf-s-offer-to-fight-terror-in-marawi-and-the-9049714
Koh Swee Lean Collin
***** ******** ***** 
***** PIE work site accident: Cracks found in corbels at 11 other parts of viaduct***** 
http://www.channelnewsasia.com/news/singapore/pie-work-site-accident-cracks-found-in-corbels-at-11-other-parts-9052886
null
***** ******** ***** 
***** National Service registration to start Aug 2***** 
http://www.channelnewsasia.com/news/singapore/national-service-registration-to-start-aug-2-9052448
null
***** ******** ***** 
***** 4 women arrested for carrying out sexual services in Ang Mo Kio, Kovan***** 
http://www.channelnewsasia.com/news/singapore/4-women-arrested-for-carrying-out-sexual-services-in-ang-mo-kio-9052120
null
***** ******** ***** 
***** New vending machine standards to address food safety, machine hygiene***** 
http://www.channelnewsasia.com/news/singapore/new-vending-machine-standards-to-address-food-safety-machine-9052100
Loh Chuan Junn
***** ******** ***** 
***** DPM Tharman on a three-day visit to India***** 
http://www.channelnewsasia.com/news/singapore/dpm-tharman-on-a-three-day-visit-to-india-9052446
null
***** ******** ***** 
***** 'Like an old friend passed on': Linkin Park fans in Singapore mourn Chester Bennington's death***** 
http://www.channelnewsasia.com/news/singapore/like-an-old-friend-passed-on-linkin-park-fans-in-singapore-mourn-9051940
Justin Ong
***** ******** ***** 
***** Jobseekers in hotel industry can now count on help from experienced career advisers***** 
http://www.channelnewsasia.com/news/singapore/jobseekers-in-hotel-industry-can-now-count-on-help-from-9051918
null
***** ******** ***** 
***** More than 100 SMEs adopt IMDA's Go Digital programme***** 
http://www.channelnewsasia.com/news/technology/more-than-100-smes-adopt-imda-s-go-digital-programme-9051866
Calvin Hui
***** ******** ***** 
***** 2 former RSAF engineers charged with corruption***** 
http://www.channelnewsasia.com/news/singapore/2-former-rsaf-engineers-charged-with-corruption-9050664
null
***** ******** ***** 
***** Saudi diplomat who sexually assaulted hotel employee has appeal dismissed***** 
http://www.channelnewsasia.com/news/singapore/saudi-diplomat-who-sexually-assaulted-hotel-employee-has-appeal-9051506
Vanessa Paige Chelvan
***** ******** ***** 
***** Singtel launches 800Mbps mobile data speeds at selected locations***** 
http://www.channelnewsasia.com/news/singapore/singtel-launches-800mbps-mobile-data-speeds-at-selected-9051388
null
***** ******** ***** 
***** Cristiano Ronaldo in Singapore to visit Peter Lim and family***** 
http://www.channelnewsasia.com/news/singapore/cristiano-ronaldo-in-singapore-to-visit-peter-lim-and-family-9050580
Aqil Haziq Mahmud
***** ******** ***** 
***** Commentary: How should Singapore teachers manage issues of race in the classroom?***** 
http://www.channelnewsasia.com/news/singapore/commentary-how-should-singapore-teachers-manage-issues-of-race-9049662
Ho Li-Ching
***** ******** ***** 
***** Singapore's value to the United States lies in its location: US ambassador nominee KT McFarland***** 
http://www.channelnewsasia.com/news/singapore/singapore-s-value-to-the-united-states-lies-in-its-location-us-9049860
null
***** ******** ***** 
***** New OneMap app launched with live traffic information***** 
http://www.channelnewsasia.com/news/singapore/new-onemap-app-launched-with-live-traffic-information-9049728
Wendy Wong
***** ******** ***** 
***** Invest in digital infrastructure to ease burden on fast-growing cities: Koh Poh Koon***** 
http://www.channelnewsasia.com/news/singapore/invest-in-digital-infrastructure-to-ease-burden-on-fast-growing-9048604
Brandon Tanoto
***** ******** ***** 
***** Two companies fined over deaths of two prime mover drivers***** 
http://www.channelnewsasia.com/news/singapore/two-companies-fined-over-deaths-of-two-prime-mover-drivers-9048622
null
***** ******** ***** 
***** Commentary: Divorce, a painful, messy and contentious affair***** 
http://www.channelnewsasia.com/news/singapore/commentary-divorce-a-painful-messy-and-contentious-affair-9047922
Koh Tien Hua
***** ******** ***** 
***** Bus driver who drank vodka at the wheel jailed and banned from driving***** 
http://www.channelnewsasia.com/news/singapore/bus-driver-who-drank-vodka-at-the-wheel-jailed-and-banned-from-9049230
Vanessa Paige Chelvan
***** ******** ***** 
***** PIE work site accident: Injured worker's condition deteriorates***** 
http://www.channelnewsasia.com/news/singapore/pie-work-site-accident-injured-worker-s-condition-deteriorates-9049212
null
***** ******** ***** 
***** Cristiano Ronaldo to visit Singapore on Friday***** 
http://www.channelnewsasia.com/news/singapore/cristiano-ronaldo-to-visit-singapore-on-friday-9048966
null
***** ******** ***** 
***** Man jailed for smuggling chewing tobacco, first conviction under amended law***** 
http://www.channelnewsasia.com/news/singapore/man-jailed-for-smuggling-chewing-tobacco-first-conviction-under-9048304
null
***** ******** ***** 
***** Chilli for a cause: 10-year-old raises funds for cancer-stricken hawker***** 
http://www.channelnewsasia.com/news/singapore/chilli-for-a-cause-10-year-old-raises-funds-for-cancer-stricken-9047432
Diane Leow
***** ******** ***** 
***** Company director fined S$3,500, penalised S$100,000 for abusing PIC scheme***** 
http://www.channelnewsasia.com/news/singapore/company-director-fined-s-3-500-penalised-s-100-000-for-abusing-9048294
null
***** ******** ***** 
***** Clubs with jackpot machines to face tighter regulations to guard against problem gambling***** 
http://www.channelnewsasia.com/news/singapore/clubs-with-jackpot-machines-to-face-tighter-regulations-to-guard-9048420
Aqil Haziq Mahmud
***** ******** ***** 
***** Man jailed 18 years for strangling elderly woman to death in botched robbery***** 
http://www.channelnewsasia.com/news/singapore/man-jailed-18-years-for-strangling-elderly-woman-to-death-in-9048180
Vanessa Paige Chelvan
***** ******** ***** 
***** Construction firm fined for illegal discharge into canal at Bukit Panjang***** 
http://www.channelnewsasia.com/news/singapore/construction-firm-fined-for-illegal-discharge-into-canal-at-9047894
null
***** ******** ***** 
***** Singaporean woman kidnapped in Manila rescued; 43 foreigners arrested***** 
http://www.channelnewsasia.com/news/singapore/singaporean-woman-kidnapped-in-manila-rescued-43-foreigners-9047096
null
***** ******** ***** 
***** East Coast Park beach reopened after oil spill clean-up***** 
http://www.channelnewsasia.com/news/singapore/east-coast-park-beach-reopened-after-oil-spill-clean-up-9047258
null
***** ******** ***** 
***** Football: Singapore crash to 2-0 defeat against Myanmar in AFC U-23 qualifiers***** 
http://www.channelnewsasia.com/news/sport/football-singapore-crash-to-2-0-defeat-against-myanmar-in-afc-u-9045980
null
***** ******** ***** 
***** PIE work site accident: MOM to look into injury compensation claims promptly, Sam Tan says***** 
http://www.channelnewsasia.com/news/singapore/pie-work-site-accident-mom-to-look-into-injury-compensation-9045820
null
***** ******** ***** 
***** Man arrested for alleged molest on board train along Yishun***** 
http://www.channelnewsasia.com/news/singapore/man-arrested-for-alleged-molest-on-board-train-along-yishun-9045392
null
***** ******** ***** 
***** 3-year-old hurt after fare gate closes on him at Hougang station***** 
http://www.channelnewsasia.com/news/singapore/3-year-old-hurt-after-fare-gate-closes-on-him-at-hougang-station-9045354
null
***** ******** ***** 
***** NHG polyclinics offer 24/7 locker service to pick up medication from 7-Eleven stores***** 
http://www.channelnewsasia.com/news/singapore/nhg-polyclinics-offer-24-7-locker-service-to-pick-up-medication-9044890
Rachelle Lee
***** ******** ***** 
***** Father jailed 2 weeks for slapping boy who 'bullied' daughter; High Court judge says fine too lenient***** 
http://www.channelnewsasia.com/news/singapore/father-jailed-2-weeks-for-slapping-boy-who-bullied-daughter-high-9045328
Vanessa Paige Chelvan
***** ******** ***** 
***** Singapore explores possibility of building a high-rise, green data centre***** 
http://www.channelnewsasia.com/news/singapore/singapore-explores-possibility-of-building-a-high-rise-green-9045112
null
***** ******** ***** 
***** Soo Kee takes a shine to China with S$10.2m joint venture***** 
http://www.channelnewsasia.com/news/singapore/soo-kee-takes-a-shine-to-china-with-s-10-2m-joint-venture-9044242
Tang See Kit
***** ******** ***** 
***** Owner of Fatty Paws Pet Shop charged for breaching licensing conditions***** 
http://www.channelnewsasia.com/news/singapore/owner-of-fatty-paws-pet-shop-charged-for-breaching-licensing-9044880
null
***** ******** ***** 
***** Mexican navy sailing ship docks at VivoCity amid much fanfare***** 
http://www.channelnewsasia.com/news/singapore/mexican-navy-sailing-ship-docks-at-vivocity-amid-much-fanfare-9044432
Gaya Chandramohan
***** ******** ***** 
***** Singtel's NetLink debuts slightly above offer price***** 
http://www.channelnewsasia.com/news/business/singtel-s-netlink-debuts-slightly-above-offer-price-9045096
null
***** ******** ***** 
***** Coca-Cola opens S$79m storage, distribution centre in Tuas***** 
http://www.channelnewsasia.com/news/singapore/coca-cola-opens-s-79m-storage-distribution-centre-in-tuas-9044572
Tang See Kit
***** ******** ***** 
***** Construction worker gets 11 years’ jail, caning for raping tourist in Geylang***** 
http://www.channelnewsasia.com/news/singapore/construction-worker-gets-11-years-jail-caning-for-raping-tourist-9044830
Vanessa Paige Chelvan
***** ******** ***** 
***** COEs mostly up in latest bidding exercise***** 
http://www.channelnewsasia.com/news/singapore/coes-mostly-up-in-latest-bidding-exercise-9043572
null
***** ******** ***** 
***** Safety system worked as designed after Kallang chemical leak: Company***** 
http://www.channelnewsasia.com/news/singapore/safety-system-worked-as-designed-after-kallang-chemical-leak-9044696
null
***** ******** ***** 
***** Josephine Teo to lead PMET job matching efforts in 5 growth sectors: Lim Swee Say***** 
http://www.channelnewsasia.com/news/singapore/josephine-teo-to-lead-pmet-job-matching-efforts-in-5-growth-9043910
Calvin Hui
***** ******** ***** 
***** HDB to offer BTO flats in Sembawang, Sengkang, Yishun with shorter waiting time***** 
http://www.channelnewsasia.com/news/singapore/hdb-to-offer-bto-flats-in-sembawang-sengkang-yishun-with-shorter-9044294
null
***** ******** ***** 
***** Suspect in Woodlands pellet-shooting incident arrested***** 
http://www.channelnewsasia.com/news/singapore/suspect-in-woodlands-pellet-shooting-incident-arrested-9044186
null
***** ******** ***** 
***** Singapore offers SAF assistance to help Philippines fight terrorism***** 
http://www.channelnewsasia.com/news/singapore/singapore-offers-saf-assistance-to-help-philippines-fight-9044142
null
***** ******** ***** 
***** SIA reviewing serving of nuts on flights following toddler's allergic reaction***** 
http://www.channelnewsasia.com/news/singapore/sia-reviewing-serving-of-nuts-on-flights-following-toddler-s-9043858
null
***** ******** ***** 
***** Taxi fares in Singapore are 20th cheapest in the world: Index***** 
http://www.channelnewsasia.com/news/singapore/taxi-fares-in-singapore-are-20th-cheapest-in-the-world-index-9043600
null
***** ******** ***** 
***** Cases of contact lenses stuck in eyes not that uncommon in Singapore, specialists say***** 
http://www.channelnewsasia.com/news/singapore/cases-of-contact-lenses-stuck-in-eyes-not-that-uncommon-in-9042330
Jalelah Abu Baker
***** ******** ***** 
***** Singapore's Rowsley to buy healthcare assets for up to S$1.9b***** 
http://www.channelnewsasia.com/news/business/singapore-s-rowsley-to-buy-healthcare-assets-for-up-to-s-1-9b-9043284
null
***** ******** ***** 
***** Afro-Asia Building to be demolished in November***** 
http://www.channelnewsasia.com/news/singapore/afro-asia-building-to-be-demolished-in-november-9042758
Brandon Tanoto
***** ******** ***** 
***** Viral video of 'kidnapped child in ice box' shows drowned boy: Reports***** 
http://www.channelnewsasia.com/news/asiapacific/viral-video-of-kidnapped-child-in-ice-box-shows-drowned-boy-9042780
null
***** ******** ***** 
***** Contractor fined S$280,000 after worker crushed to death by toppled gate at Bartley Road***** 
http://www.channelnewsasia.com/news/singapore/contractor-fined-s-280-000-after-worker-crushed-to-death-by-9042354
null
***** ******** ***** 
***** Ban on display of tobacco products to take effect on Aug 1 as grace period ends***** 
http://www.channelnewsasia.com/news/singapore/ban-on-display-of-tobacco-products-to-take-effect-on-aug-1-as-9041262
null
***** ******** ***** 
***** M1's profit falls 17.6% year-on-year for first half of 2017***** 
http://www.channelnewsasia.com/news/singapore/m1-s-profit-falls-17-6-year-on-year-for-first-half-of-2017-9041920
null
***** ******** ***** 
***** Learn how to pair hawker food with wine at the Singapore Food Festival’s Hawker Wine Safari***** 
http://www.channelnewsasia.com/news/lifestyle/learn-how-to-pair-hawker-food-with-wine-at-the-singapore-food-9041574
Genevieve Sarah Loh
***** ******** ***** 
***** Lorry flips over at Yishun; no injuries reported***** 
http://www.channelnewsasia.com/news/singapore/lorry-flips-over-at-yishun-no-injuries-reported-9041948
null
***** ******** ***** 
***** Singaporean undergraduate wins international game design contest***** 
http://www.channelnewsasia.com/news/singapore/singaporean-undergraduate-wins-international-game-design-contest-9041184
Noor Farhan
***** ******** ***** 
***** Government to exhume over 80,000 graves, acquire land to make way for Tengah Air Base expansion***** 
http://www.channelnewsasia.com/news/singapore/government-to-exhume-over-80-000-graves-acquire-land-to-make-way-9041536
Jalelah Abu Baker
***** ******** ***** 
***** MOH incurred S$4.08m expenditure without verifying need: AGO report***** 
http://www.channelnewsasia.com/news/singapore/moh-incurred-s-4-08m-expenditure-without-verifying-need-ago-9041346
null
***** ******** ***** 
***** Head of civil service Peter Ong to retire on Sep 1, Leo Yip to take over***** 
http://www.channelnewsasia.com/news/singapore/head-of-civil-service-peter-ong-to-retire-on-sep-1-leo-yip-to-9041524
null
***** ******** ***** 
***** 11 people taken to hospital after Kallang Way chemical leak***** 
http://www.channelnewsasia.com/news/singapore/11-people-taken-to-hospital-after-kallang-way-chemical-leak-9041318
null
***** ******** ***** 
***** SSC made late payments worth S$661,900 without a valid reason: AGO report***** 
http://www.channelnewsasia.com/news/singapore/ssc-made-late-payments-without-valid-reason-ago-report-9041076
null
***** ******** ***** 
***** AGO's report on CPF Board: Lapses in management of 2 IT security monitoring systems***** 
http://www.channelnewsasia.com/news/singapore/ago-s-report-on-cpf-board-lapses-in-management-of-2-it-security-9041116
null
***** ******** ***** 
***** 11 critical information infrastructure sectors tested for first time in national cybersecurity exercise***** 
http://www.channelnewsasia.com/news/singapore/11-critical-information-infrastructure-sectors-tested-for-first-9040914
Loh Chuan Junn
***** ******** ***** 
***** PUB inks agreements to deepen innovation in water technologies***** 
http://www.channelnewsasia.com/news/singapore/pub-inks-agreements-to-deepen-innovation-in-water-technologies-9040728
Vanessa Lim
***** ******** ***** 
***** HSA issues alert on cosmetic set containing 'very high mercury levels', prohibited ingredients***** 
http://www.channelnewsasia.com/news/singapore/hsa-issues-alert-on-cosmetic-set-containing-very-high-mercury-9040670
null
***** ******** ***** 
***** Lax oversight in IT, financial controls uncovered in Auditor-General's report***** 
http://www.channelnewsasia.com/news/singapore/oversight-it-financial-controls-uncovered-in-ago-report-9040668
null
***** ******** ***** 
***** PIE work site accident: MWC says it faced delays in meeting affected workers***** 
http://www.channelnewsasia.com/news/singapore/pie-work-site-accident-mwc-says-it-faced-delays-in-meeting-9040496
null
***** ******** ***** 
***** Fewer marriages, more divorces in 2016: Singstat***** 
http://www.channelnewsasia.com/news/singapore/fewer-marriages-more-divorces-in-2016-singstat-9040550
null
***** ******** ***** 
***** Commentary: The age-old question about university rankings***** 
http://www.channelnewsasia.com/news/singapore/commentary-the-age-old-question-about-university-rankings-9031572
Pan Zaixian
***** ******** ***** 
***** More than 400 safety violations found at work sites in June: MOM***** 
http://www.channelnewsasia.com/news/singapore/more-than-400-safety-violations-found-at-work-sites-in-june-mom-9039440
Lee Li Ying
***** ******** ***** 
***** Defence Minister Ng Eng Hen to visit Philippines***** 
http://www.channelnewsasia.com/news/singapore/defence-minister-ng-eng-hen-to-visit-philippines-9038950
null
***** ******** ***** 
***** Dry and warm weather forecast for second half of July***** 
http://www.channelnewsasia.com/news/singapore/dry-and-warm-weather-forecast-for-second-half-of-july-9038892
null
***** ******** ***** 
***** Singapore youth footballer signs for English club Fulham***** 
http://www.channelnewsasia.com/news/singapore/singapore-youth-footballer-signs-for-english-club-fulham-9038832
null
***** ******** ***** 
***** Woman arrested for online scam involving USS tickets, staycation deals***** 
http://www.channelnewsasia.com/news/singapore/woman-arrested-for-online-scam-involving-uss-tickets-staycation-9038958
null
***** ******** ***** 
***** Full speech: Vivian Balakrishnan highlights principles underpinning Singapore's diplomacy***** 
http://www.channelnewsasia.com/news/singapore/full-speech-vivian-balakrishnan-highlights-principles-9038582
null
***** ******** ***** 
***** Singaporean contestant Joanna Dong 'moved' to be chosen by Jay Chou in Sing! China***** 
http://www.channelnewsasia.com/news/lifestyle/joanna-dong-moved-to-be-chosen-jay-chou-sing-china-9038666
Genevieve Sarah Loh
***** ******** ***** 
***** More resources committed to search for missing Singaporean diver: MFA***** 
http://www.channelnewsasia.com/news/singapore/more-resources-committed-to-search-for-missing-singaporean-diver-9038864
null
***** ******** ***** 
